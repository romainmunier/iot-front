import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Statistics from "@/views/Statistics";
import Authentication from "@/views/Authentication";
import Register from "@/views/Register";
import Params from "@/views/Params";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/statistics',
    name: "Statistics",
    component: Statistics
  },
  {
    path: '/params',
    name: "Params",
    component: Params
  },
  {
    path: "/auth",
    name: "Authentication",
    component: Authentication
  },
  {
    path: "/register",
    name: "Register",
    component: Register
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
