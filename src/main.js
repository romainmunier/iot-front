import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import "./assets/js/bootstrap.min"

let VueApp = createApp(App)

import {
  Chart, ArcElement, LineElement,
  BarElement, PointElement, BarController,
  BubbleController, DoughnutController, LineController,
  PieController, PolarAreaController, RadarController,
  ScatterController, CategoryScale, LinearScale,
  LogarithmicScale, RadialLinearScale, TimeScale,
  TimeSeriesScale, Decimation, Filler,
  Legend, Title, Tooltip, SubTitle

} from 'chart.js';

Chart.register(
  ArcElement, LineElement, BarElement,
  PointElement, BarController, BubbleController,
  DoughnutController, LineController, PieController,
  PolarAreaController, RadarController, ScatterController,
  CategoryScale, LinearScale, LogarithmicScale,
  RadialLinearScale, TimeScale, TimeSeriesScale,
  Decimation, Filler, Legend,
  Title, Tooltip, SubTitle
);

// import VueSocketio from 'vue-socket.io';
// import io from "socket.io-client"
//
// const SocketInstance = io.connect("http://localhost:3000/", {
//   query: {
//     token: window.localStorage.getItem('auth')
//   },
//   withCredentials: false
// });
//
// VueApp.use(new VueSocketio({
//   debug: true,
//   connection: SocketInstance
// }));

import VueSocketIO from 'vue-3-socket.io'
import SocketIO from 'socket.io-client'

VueApp.use(new VueSocketIO({
    debug: true,
    connection: SocketIO('http://localhost:3000/'), //options object is Optional
    vuex: {
      store,
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_"
    }
  })
);

const API = require("./config/API.json")
VueApp.config.globalProperties.$API = API

VueApp.config.globalProperties.$ChartJS = Chart

VueApp.use(store)
  .use(router)
  .mount("#app")
